//
//  NetworkingWrapper.swift
//  ForexPairs
//
//  Created by Alexandre Camilleri on 19/02/2019.
//  Copyright © 2019 Alexandre Camilleri. All rights reserved.
//

import Networking
import Model

enum FFANetworkingError: Error {
    case invalidUrlParameters
    case dataDecoding
    case networking(underlyingError: NetworkingError)
}

/// FFA stands for Free Forex API - see https://www.freeforexapi.com for more details
struct FFANetworking {
    
    typealias CurrencyPairsRateHandler = (Result<CurrencyPairsRate, FFANetworkingError>) -> Void
    
    typealias CurrencyPairsListHandler  = (Result<[CurrencyPairName], FFANetworkingError>) -> Void
    
    // MARK: - Contants
    
    static let endpointUrl = URL(string: "https://www.freeforexapi.com/api/live")!
    
    // MARK: - Properties
    
    let networking: NetworkingProtocol
    
    let decoder: JSONDecoder
    
    // MARK: - Public Methods
    
    public init(networking: NetworkingProtocol, decoder: JSONDecoder) {
        self.networking = networking
        self.decoder = decoder
    }
    
    /// Retrieve the rate for a given list of currency pairs
    /// Call this function when you need to refresh the rates of a list of supported currency pairs
    ///
    /// - Parameters:
    ///   - currencyPairsList: The array of CurrencyPairName to fetch rate for
    ///   - handler: The completion handler
    public func getRates(for currencyPairsList: [CurrencyPairName], handler: @escaping CurrencyPairsRateHandler) {
        guard let urlComponents = NSURLComponents(string: FFANetworking.endpointUrl.absoluteString) else {
            fatalError("Invalid Endpoint URL for Free Forex API")
        }
        // Add parameter to the endpoint in order to retrieve the given pairs' rates
        urlComponents.queryItems = [URLQueryItem(name: "pairs", value: currencyPairsList.joined(separator: ","))]
        guard let urlWithParameter = urlComponents.url else {
            handler(.failure(.invalidUrlParameters))
            return
        }
        var request = URLRequest(url: urlWithParameter)
        request.httpMethod = HttpMethod.get.rawValue
        
        networking.run(request) { result in
            switch result {
            case .failure(let error):
                handler(.failure(.networking(underlyingError: error)))
            case .success(let data):
                guard let response = try? self.decoder.decode(RateResponse.self, from: data) else {
                    handler(.failure(.dataDecoding))
                    return
                }
                handler(.success(response.rates))
            }
        }
    }
    
    /// Retrieve the list of supported currency pairs
    /// Call this function at application start and refresh it at each launch
    ///
    /// - Parameter handler: The completion handler
    public func getCurrencyPairsList(handler: @escaping CurrencyPairsListHandler) {
        var request = URLRequest(url: FFANetworking.endpointUrl)
        request.httpMethod = HttpMethod.get.rawValue
        
        networking.run(request) { result in
            switch result {
            case .failure(let error):
                handler(.failure(.networking(underlyingError: error)))
            case .success(let data):
                guard let response = try? self.decoder.decode(CurrencyPairsListResponse.self, from: data) else {
                    handler(.failure(.dataDecoding))
                    return
                }
                handler(.success(response.currencyPairs))
            }
        }
    }
}
