//
//  ForexDataStorage.swift
//  ForexPairs
//
//  Created by Alexandre Camilleri on 19/02/2019.
//  Copyright © 2019 Alexandre Camilleri. All rights reserved.
//

import Model
import Networking

enum ForexDataStorageError: Error {
    case currencyPairsListRequest
    case currencyPairsRateRequest
    case ffaNetworking(underlyingError: FFANetworkingError)
}

/// Used to store and update Forex app data
final class ForexDataStorage {
    
    typealias CompletionHandler = (Result<Void, ForexDataStorageError>) -> Void
    
    // MARK: - Constants
    
    static let supportedCurrencyPairsKey = "forexPairs-supported-currency-pairs"
    
    static let initializationCurrencyPairsRate = "forexPairs-initialization-currency-pairs-rate"
    
    // MARK: - Properties
    
    /// Cache for the list of supported currency pairs stored in UserDefaults
    var currencyPairsList: [CurrencyPairName] {
        get { return UserDefaults.standard.array(forKey: ForexDataStorage.supportedCurrencyPairsKey) as? [CurrencyPairName] ?? [] }
        set { UserDefaults.standard.setValue(newValue, forKey: ForexDataStorage.supportedCurrencyPairsKey) }
    }
    
    #warning("not elegant but ran into trouble with storing custom type :(")
    var initializationCurrencyPairsRate: [CurrencyPairName: Double] {
        get {
            return UserDefaults.standard.dictionary(forKey: ForexDataStorage.initializationCurrencyPairsRate) as? [CurrencyPairName: Double] ?? [:]
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: ForexDataStorage.initializationCurrencyPairsRate)
        }
    }
    
    /// List of supported currency pairs with their associated rates
    /// Notitify delegate on update
    var currencyPairsRate: CurrencyPairsRate = [:]
    
    // MARK: - Methods
    
    func initialize(using ffaNetworking: FFANetworking, handler: @escaping CompletionHandler) {
        fetchCurrencyPairsList(using: ffaNetworking) { result in
            switch result {
            case .success():
                self.fetchCurrencyPairsRate(using: ffaNetworking) { result in
                    switch result {
                    case .success():
                        
                        
                        
                        var initializationCurrencyPairsRate = [CurrencyPairName: Double]()
                        self.currencyPairsRate.forEach({ (currencyPairRate) in
                            let (currencyPairName, rate) = currencyPairRate
                            initializationCurrencyPairsRate[currencyPairName] = rate.value
                        })
                        self.initializationCurrencyPairsRate = initializationCurrencyPairsRate
                        
                    
                        
                        
                        handler(.success(()))
                    case .failure(_):
                        handler(.failure(.currencyPairsRateRequest))
                    }
                }
            case .failure(_):
                handler(.failure(.currencyPairsListRequest))
            }
        }
    }
    
    /// Updated the current `currencyPairsList` - Notify caller with optionnal handler
    ///
    /// - Parameters:
    ///   - ffaNetworking: An instance of FFANetworking
    ///   - handler: Optional handler to monitor the outcome
    func fetchCurrencyPairsList(using ffaNetworking: FFANetworking, handler: CompletionHandler? = nil) {
        ffaNetworking.getCurrencyPairsList() { result in
            switch result {
            case .failure(let error):
                handler?(.failure(.ffaNetworking(underlyingError: error)))
            case .success(let currencyPairsList):
                self.currencyPairsList = currencyPairsList
                handler?(.success(()))
            }
        }
    }

    func fetchCurrencyPairsRate(using ffaNetworking: FFANetworking, handler: CompletionHandler? = nil) {
        ffaNetworking.getRates(for: currencyPairsList) { result in
            switch result {
            case .failure(let error):
                handler?(.failure(.ffaNetworking(underlyingError: error)))
            case .success(let currencyPairsRate):
                self.currencyPairsRate = currencyPairsRate
                handler?(.success(()))
            }
        }
    }
}
