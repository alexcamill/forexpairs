//
//  StartupDataLoadingViewController.swift
//  ForexPairs
//
//  Created by Alexandre Camilleri on 20/02/2019.
//  Copyright © 2019 Alexandre Camilleri. All rights reserved.
//

import UIKit

class StartupDataLoadingViewController: UIViewController {

    @IBOutlet weak var label: UILabel!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        modalTransitionStyle = .crossDissolve
        label.text = NSLocalizedString("STARTUP_DATA_LOADING_LABEL", comment: "")
    }
    
    static func instantiate() -> StartupDataLoadingViewController {
        let storyboard = UIStoryboard.init(name: "StartupDataLoading", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "StartupDataLoadingViewController") as! StartupDataLoadingViewController
    }
}
