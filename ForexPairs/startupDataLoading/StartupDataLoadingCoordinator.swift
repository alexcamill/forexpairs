//
//  StartupDataLoadingCoordinator.swift
//  ForexPairs
//
//  Created by Alexandre Camilleri on 20/02/2019.
//  Copyright © 2019 Alexandre Camilleri. All rights reserved.
//

import UIKit
import Networking

final class StartupDataLoadingCoordinator: Coordinator {

    // MARK: Properties
    
    let presenter: UINavigationController
    
    let homeScreenCoordinator: HomeScreenCoordinator
    
    var startupDataLoadingViewController: StartupDataLoadingViewController?
    
    let networking: Networking
    
    let ffaNetworking: FFANetworking
    
    let forexDataStorage: ForexDataStorage
    
    // MARK: - Methods
    
    init(presenter: UINavigationController, networking: Networking) {
        self.presenter = presenter
        self.networking = networking
        
        ffaNetworking = FFANetworking(networking: networking, decoder: JSONDecoder())
        forexDataStorage = ForexDataStorage()
        homeScreenCoordinator = HomeScreenCoordinator.init(presenter: presenter,
                                                           ffaNetworking: ffaNetworking,
                                                           forexDataStorage: forexDataStorage)
    }
    
    func start() {
        let startupDataLoadingViewController = StartupDataLoadingViewController.instantiate()
        
        presenter.present(startupDataLoadingViewController, animated: true, completion: nil)
        self.startupDataLoadingViewController = startupDataLoadingViewController
        
        // Fetch remote data
        DispatchQueue.global(qos: .background).async { self.fetchForexData() }
    }
    
    // MARK: - Private Methods
    
    private func fetchForexData() {
        forexDataStorage.initialize(using: ffaNetworking) { result in
            switch result {
            case .success():
            DispatchQueue.main.async {
                self.homeScreenCoordinator.start()
            }
            case .failure(_):
                print("Failed loading data")
                #warning("Do proper error handling here")
            }
        }
    }
}
