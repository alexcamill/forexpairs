//
//  ApplicationCoordinator.swift
//  ForexPairs
//
//  Created by Alexandre Camilleri on 19/02/2019.
//  Copyright © 2019 Alexandre Camilleri. All rights reserved.
//

import UIKit
import Networking

final class ApplicationCoordinator: Coordinator {
    
    let window: UIWindow
    
    let rootViewController: UINavigationController
    
    let startupDataLoadingCoordinator: StartupDataLoadingCoordinator
    
    let networking: Networking

    init(window: UIWindow) {
        self.window = window
        rootViewController = UINavigationController()
        networking = Networking()
        startupDataLoadingCoordinator = StartupDataLoadingCoordinator(presenter: rootViewController,
                                                                      networking: networking)
        setNavigationBarAppearance()
        setTabBarAppearance()
        setTabBarItemAppearance()
    }
    
    func start() {
        window.rootViewController = rootViewController
        window.makeKeyAndVisible()
        startupDataLoadingCoordinator.start()
    }
    
    private func setNavigationBarAppearance() {
        let navigationBarAppearace = UINavigationBar.appearance()
        navigationBarAppearace.tintColor = UIColor(named: "navigation-bar")
        navigationBarAppearace.barTintColor = UIColor(named: "navigation-bar")
        navigationBarAppearace.barStyle = .blackTranslucent
    }
    
    private func setTabBarAppearance() {
        let tabBarAppearance = UITabBar.appearance()
        tabBarAppearance.barTintColor = UIColor(named: "tabbar-background")
        tabBarAppearance.tintColor = UIColor.white
        tabBarAppearance.isTranslucent = true
    }
    
    private func setTabBarItemAppearance() {
        let tabBarItemAppearance = UITabBarItem.appearance()
        tabBarItemAppearance.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.init(named: "cloud")!],
                                                    for: .normal)
        tabBarItemAppearance.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white],
                                                    for: .selected)
    }
}
