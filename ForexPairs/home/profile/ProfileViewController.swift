//
//  ProfileViewController.swift
//  ForexPairs
//
//  Created by Alexandre Camilleri on 20/02/2019.
//  Copyright © 2019 Alexandre Camilleri. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = NSLocalizedString("PORTFOLIO_SECTION_TITLE", comment: "")
    }
    
    static func instantiate() -> ProfileViewController {
        let storyboard = UIStoryboard(name: "Tabs", bundle: nil)
        let profileViewController = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        
        profileViewController.tabBarItem.title = NSLocalizedString("PORTFOLIO_SECTION_TITLE", comment: "")
        profileViewController.tabBarItem.image = UIImage(named: "profile")
        return profileViewController
    }
}
