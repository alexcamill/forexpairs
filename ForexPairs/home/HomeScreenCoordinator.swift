//
//  HomeScreenCoordinator.swift
//  ForexPairs
//
//  Created by Alexandre Camilleri on 19/02/2019.
//  Copyright © 2019 Alexandre Camilleri. All rights reserved.
//

import UIKit
import Networking
import Model

/// Coordinator of the landing screen of the App
final class HomeScreenCoordinator: NSObject, Coordinator {
    
    // MARK: Properties
    
    let presenter: UINavigationController
    
    var homeScreenViewController: HomeScreenViewController?
    
    var homeScreenTabBarController: UITabBarController?
    
    var marketsViewController: MarketsViewController?
    
    var portfolioViewController: PortfolioViewController?
    
    var chatViewController: ChatViewController?
    
    var profileViewController: ProfileViewController?
    
    let ffaNetworking: FFANetworking
    
    let forexDataStorage: ForexDataStorage
    
    // MARK: - Methods
    
    init(presenter: UINavigationController, ffaNetworking: FFANetworking, forexDataStorage: ForexDataStorage) {
        self.presenter = presenter
        self.ffaNetworking = ffaNetworking
        self.forexDataStorage = forexDataStorage
    }
    
    func start() {
        // Instantiate tab's viewControllers
        let marketsViewController = MarketsViewController.instantiate()
        let portfolioViewController = PortfolioViewController.instantiate()
        let chatViewController = ChatViewController.instantiate()
        let profileViewController = ProfileViewController.instantiate()
        
        marketsViewController.delegate = self
        
        // Instantiate TabBarController
        let tabBarViewControllers = [marketsViewController,
                                     portfolioViewController,
                                     chatViewController,
                                     profileViewController]
        let tabBarController = instantiateTabBarController(displaying: tabBarViewControllers)
        
        let homeScreenViewController = HomeScreenViewController.instantiate(tabBarController: tabBarController)
        presenter.pushViewController(homeScreenViewController, animated: true)
        // Dismiss modal loader - This may not respect the architecture much - to change
        presenter.dismiss(animated: true, completion: nil)
        
        self.marketsViewController = marketsViewController
        self.portfolioViewController = portfolioViewController
        self.chatViewController = chatViewController
        self.profileViewController = profileViewController
        self.homeScreenViewController = homeScreenViewController
        self.homeScreenTabBarController = tabBarController
    }
    
    private func instantiateTabBarController(displaying viewcontrollers: [UIViewController]) -> UITabBarController {
        let tabBarController = UITabBarController(nibName: nil, bundle: nil)

        tabBarController.viewControllers = viewcontrollers
        tabBarController.delegate = self
        return tabBarController
    }
    
}

extension HomeScreenCoordinator: UITabBarControllerDelegate {

    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        print("Hello")
    }

}

extension HomeScreenCoordinator: MarketViewControllerDelegate {
    var currencyPairsRateValueOpening: [CurrencyPairName: Double] {
        return forexDataStorage.initializationCurrencyPairsRate
    }
    
    var currencyPairsRate: CurrencyPairsRate {
        return forexDataStorage.currencyPairsRate
    }
    
    func updatedCurrencyPairsRate(handler: @escaping (Result<Void, ForexDataStorageError>) -> Void) {
        forexDataStorage.fetchCurrencyPairsRate(using: ffaNetworking) { result in
            switch result {
            case .success():
                handler(.success(()))
            case .failure(let error):
                handler(.failure(error))
            }
        }
    }
}
