//
//  MarketsViewController.swift
//  ForexPairs
//
//  Created by Alexandre Camilleri on 19/02/2019.
//  Copyright © 2019 Alexandre Camilleri. All rights reserved.
//

import UIKit
import Model

protocol MarketViewControllerDelegate: class {
    var currencyPairsRate: CurrencyPairsRate { get }
    var currencyPairsRateValueOpening: [CurrencyPairName: Double] { get }
}

final class MarketsViewController: UIViewController {

    @IBOutlet weak var currencyPairsRateContainer: UIView!
    
    weak var delegate: MarketViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = NSLocalizedString("MARKETS_SECTION_TITLE", comment: "")
        
        guard let currencyPairsRate = delegate?.currencyPairsRate else { return }
        guard let currencyPairsRateValueOpening = delegate?.currencyPairsRateValueOpening  else { return }
        
        let currencyPairsRateTableviewController = CurrencyPairsRateTableViewController.instantiate(currencyPairsRate: currencyPairsRate,
                                                                                                    currencyPairsRateValueOpening: currencyPairsRateValueOpening)
        
        // Add child view controller to container
        addChild(currencyPairsRateTableviewController)
        currencyPairsRateTableviewController.view.translatesAutoresizingMaskIntoConstraints = false
        currencyPairsRateContainer.addSubview(currencyPairsRateTableviewController.view)
        NSLayoutConstraint.activate([
            currencyPairsRateTableviewController.view.leadingAnchor.constraint(equalTo: currencyPairsRateContainer.leadingAnchor),
            currencyPairsRateTableviewController.view.trailingAnchor.constraint(equalTo: currencyPairsRateContainer.trailingAnchor),
            currencyPairsRateTableviewController.view.topAnchor.constraint(equalTo: currencyPairsRateContainer.topAnchor),
            currencyPairsRateTableviewController.view.bottomAnchor.constraint(equalTo: currencyPairsRateContainer.bottomAnchor)
            ])
        currencyPairsRateTableviewController.didMove(toParent: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    static func instantiate() -> MarketsViewController {
        let storyboard = UIStoryboard(name: "Tabs", bundle: nil)
        let marketsViewController = storyboard.instantiateViewController(withIdentifier: "MarketsViewController") as! MarketsViewController
        marketsViewController.tabBarItem.title = NSLocalizedString("MARKETS_SECTION_TITLE", comment: "")
        marketsViewController.tabBarItem.image = UIImage(named: "markets")
        return marketsViewController
    }

}
