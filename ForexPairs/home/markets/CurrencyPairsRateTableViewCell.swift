//
//  CurrencyPairsRateTableViewCell.swift
//  ForexPairs
//
//  Created by Alexandre Camilleri on 20/02/2019.
//  Copyright © 2019 Alexandre Camilleri. All rights reserved.
//

import UIKit
import Model

class CurrencyPairsRateTableViewCell: UITableViewCell {
    
    // MARK: - Constants
    
    static let reuseIdentifier = "CurrencyPairsRateTableViewCell"
    
    var currencyPairName: CurrencyPairName?
    
    var rate: Rate?
    
    // MARK: - IB Outlets
    
    @IBOutlet weak var pairLabel: UILabel!
    
    @IBOutlet weak var deltaLabel: UILabel!
    
    @IBOutlet weak var sellLabel: UILabel!
    
    @IBOutlet weak var buyLabel: UILabel!
    
    // MARK: - Methods
    
    func configure(using currencyPairName: CurrencyPairName, currentRate rate: Rate, delta: Double) {
        pairLabel.text = currencyPairName
        deltaLabel.attributedText = format(delta: delta)
        #warning("Replace nasty abs - just for demo")
        sellLabel.text = format(price: abs(rate.value + Double.random(in: 0...5)))
        buyLabel.text = format(price: abs(rate.value - Double.random(in: 0...5)))
        #warning("Should change model to store both pairs instead of doing things like that after")
//        let firstPair = currencyPairName[currencyPairName.startIndex..<currencyPairName.startIndex + 3]
//        let secondPair = currencyPairName[currencyPairName.startIndex..<currencyPairName.startIndex + 3]
//        self.currencyPairName = "\()/\(currencyPairName[3..<6])"
        self.currencyPairName = currencyPairName
        self.rate = rate
    }
    
    private func format(delta: Double) -> NSAttributedString {
        let deltaPercentageString = "\(abs(delta) * 100)%"
        let colorAttribute: [NSAttributedString.Key: Any]
    
        switch delta.sign {
        case .minus:
            colorAttribute = [.foregroundColor: UIColor.red]
        case .plus:
            colorAttribute = [.foregroundColor: UIColor.green]
        }
        return NSAttributedString(string: deltaPercentageString, attributes: colorAttribute)
    }
    
    #warning("Replace by proper number formatter")
    private func format(price: Double) -> String {
        switch String((price * 10000).rounded() / 10000).count {
        case 0...7:
            return String(format: "%.3f", price)
        case 7...8:
            return String(format: "%.2f", price)
        case 8...9:
            return String(format: "%.1f", price)
        default:
            return String(format: "%.0f", price)
        }
        
    }

}
