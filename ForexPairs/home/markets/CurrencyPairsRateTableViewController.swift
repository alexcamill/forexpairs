//
//  CurrencyPairsRateTableViewController.swift
//  ForexPairs
//
//  Created by Alexandre Camilleri on 20/02/2019.
//  Copyright © 2019 Alexandre Camilleri. All rights reserved.
//

import UIKit
import Model

class CurrencyPairsRateTableViewController: UITableViewController {
    
    var currencyPairsRateValueOpening: [CurrencyPairName: Double] = [:]
    
    var currencyPairsRate: [(CurrencyPairName, Rate)] = []
    
    var currencyPairsDelta: [CurrencyPairName: Double] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = 60
    }
    
    func update(currencyPairsRate: CurrencyPairsRate) {
        // Should change the type instead but not much time left...
        self.currencyPairsRate = currencyPairsRate.flatMap { ($0.key, $0.value) }.sorted { $0.0 > $1.0 }
        
        // Calculate delta from opening rates
        var deltas = [CurrencyPairName: Double]()
        currencyPairsRate.forEach { currencyPairRate in
            let (currencyPairName, rate) = currencyPairRate
            guard let currencyPairRateValueOpening = currencyPairsRateValueOpening[currencyPairName] else { return }
            deltas[currencyPairName] = (rate.value - currencyPairRateValueOpening) / currencyPairRateValueOpening
        }
        self.currencyPairsDelta = deltas
        
        tableView.reloadData()
    }
    
    static func instantiate(currencyPairsRate: CurrencyPairsRate, currencyPairsRateValueOpening: [CurrencyPairName: Double]) -> CurrencyPairsRateTableViewController {
        let storyboard = UIStoryboard(name: "Tabs", bundle: nil)
        let currencyPairsRateTableViewController = storyboard.instantiateViewController(withIdentifier: "CurrencyPairsRateTableViewController") as! CurrencyPairsRateTableViewController
        
        
        currencyPairsRateTableViewController.currencyPairsRateValueOpening = currencyPairsRateValueOpening
        currencyPairsRateTableViewController.update(currencyPairsRate: currencyPairsRate)
        return currencyPairsRateTableViewController
    }
}

extension CurrencyPairsRateTableViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currencyPairsRate.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = CurrencyPairsRateTableViewCell.reuseIdentifier
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? CurrencyPairsRateTableViewCell else {
            fatalError("Cell identifier \(cellIdentifier) missing")
        }
        let (currencyPairName, rate) = currencyPairsRate[indexPath.row]
        guard let openingRate = currencyPairsDelta[currencyPairName] else { fatalError("Opening rate missing") } // Should not fatal error
        cell.configure(using: currencyPairName, currentRate: rate, delta: openingRate)
        return cell
    }
}
