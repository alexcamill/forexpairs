//
//  ChatViewController.swift
//  ForexPairs
//
//  Created by Alexandre Camilleri on 20/02/2019.
//  Copyright © 2019 Alexandre Camilleri. All rights reserved.
//

import UIKit

class ChatViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = NSLocalizedString("PORTFOLIO_SECTION_TITLE", comment: "")
    }
    
    static func instantiate() -> ChatViewController {
        let storyboard = UIStoryboard(name: "Tabs", bundle: nil)
        let chatViewController = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        
        chatViewController.tabBarItem.title = NSLocalizedString("PORTFOLIO_SECTION_TITLE", comment: "")
        chatViewController.tabBarItem.image = UIImage(named: "chat")
        return chatViewController
    }
}
