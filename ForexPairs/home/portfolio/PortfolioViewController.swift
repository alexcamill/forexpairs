//
//  PortfolioViewController.swift
//  ForexPairs
//
//  Created by Alexandre Camilleri on 20/02/2019.
//  Copyright © 2019 Alexandre Camilleri. All rights reserved.
//

import UIKit

class PortfolioViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = NSLocalizedString("PORTFOLIO_SECTION_TITLE", comment: "")
    }
    
    static func instantiate() -> PortfolioViewController {
        let storyboard = UIStoryboard(name: "Tabs", bundle: nil)
        let portfolioViewController = storyboard.instantiateViewController(withIdentifier: "PortfolioViewController") as! PortfolioViewController
        
        portfolioViewController.tabBarItem.title = NSLocalizedString("PORTFOLIO_SECTION_TITLE", comment: "")
        portfolioViewController.tabBarItem.image = UIImage(named: "portfolio")
        return portfolioViewController
    }
}
