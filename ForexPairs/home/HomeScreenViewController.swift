//
//  HomeScreenViewController.swift
//  ForexPairs
//
//  Created by Alexandre Camilleri on 20/02/2019.
//  Copyright © 2019 Alexandre Camilleri. All rights reserved.
//

import UIKit

class HomeScreenViewController: UIViewController {
    
    // MARK: Properties
    
    var container: UIView?
    
    var homeScreenTabBarController: UITabBarController?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let tabBarController = homeScreenTabBarController else { return }
        
        // Add container
        let container = UIView()
        container.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(container)
        NSLayoutConstraint.activate([
            container.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            container.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            container.topAnchor.constraint(equalTo: view.topAnchor, constant: 0),
            container.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            ])
        
        // Add child view controller to container
        addChild(tabBarController)
        tabBarController.view.translatesAutoresizingMaskIntoConstraints = false
        container.addSubview(tabBarController.view)
        NSLayoutConstraint.activate([
            tabBarController.view.leadingAnchor.constraint(equalTo: container.leadingAnchor),
            tabBarController.view.trailingAnchor.constraint(equalTo: container.trailingAnchor),
            tabBarController.view.topAnchor.constraint(equalTo: container.topAnchor),
            tabBarController.view.bottomAnchor.constraint(equalTo: container.bottomAnchor)
            ])
        tabBarController.didMove(toParent: self)
        
        configureSearchBarButtonItem()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationItem.setHidesBackButton(true, animated: animated)
    }
    
    // MARK: - Private Methods
    
    private func configureSearchBarButtonItem() {
        let searchBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.search,
                                                  target: nil, action: nil)
        searchBarButtonItem.tintColor = UIColor.init(named: "cloud")
        navigationItem.rightBarButtonItems = [searchBarButtonItem]
    }
    
    // MARK: - Static Functions
    
    static func instantiate(tabBarController: UITabBarController) -> HomeScreenViewController {
        let storyboard = UIStoryboard(name: "HomeScreen", bundle: nil)
        let homeScreenViewController = storyboard.instantiateViewController(withIdentifier: "HomeScreenViewController") as! HomeScreenViewController
        
        homeScreenViewController.homeScreenTabBarController = tabBarController
        return homeScreenViewController
    }

}
