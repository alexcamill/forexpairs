//
//  ForexDataStorageTests.swift
//  ForexPairsTests
//
//  Created by Alexandre Camilleri on 19/02/2019.
//  Copyright © 2019 Alexandre Camilleri. All rights reserved.
//

import XCTest
@testable import ForexPairs
import Networking

#warning("Not happy with these, need a rework")
class ForexDataStorageTests: XCTestCase {

    func testInitialize() {
        let jsonDecoder = JSONDecoder()
        let networking = Networking()
        let ffaNetworking = FFANetworking(networking: networking, decoder: jsonDecoder)
        let forexDataStorage = ForexDataStorage.init()
        
        let expectation = self.expectation(description: "Waiting for forexDataStorage init...")
        
        forexDataStorage.initialize(using: ffaNetworking) { result in
            switch result {
            case .failure(let error):
                XCTFail("error: \(error)")
            case .success():
                expectation.fulfill()
            }
            
        }
        waitForExpectations(timeout: 2) { _ in
            XCTAssertGreaterThanOrEqual(forexDataStorage.currencyPairsList.count, 1)
            XCTAssertTrue(forexDataStorage.currencyPairsRate["EURUSD"] != nil, "EURUSD pair missing")
        }
    }

}
