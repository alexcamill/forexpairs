//
//  Networking.swift
//  ForexPairs
//
//  Created by Alexandre Camilleri on 19/02/2019.
//  Copyright © 2019 Alexandre Camilleri. All rights reserved.
//

import Foundation
import Model

public enum HttpMethod: String {
    case get = "GET"
}

public enum NetworkingError: Error {
    case requestError(error: Error)
    case noData
}

public struct Networking: NetworkingProtocol {
    
    var session: URLSession {
        let config = URLSessionConfiguration.default
        return URLSession(configuration: config)
    }
    
    // MARK: - Methods
    
    public init() {}
    
    public func run(_ request: URLRequest, completion handler: @escaping NetworkingHandler) {
        let dataTask = session.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print("Error calling GET on \(String(describing: request.url)): \(error!)")
                handler(.failure(.requestError(error: error!)))
                return
            }
            guard let responseData = data else {
                handler(.failure(.noData))
                print("Error: did not receive data")
                return
            }
            handler(.success(responseData))
        }
        dataTask.resume()
    }
}
