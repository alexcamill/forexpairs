//
//  NetworkingProtocol.swift
//  Networking
//
//  Created by Alexandre Camilleri on 19/02/2019.
//  Copyright © 2019 Alexandre Camilleri. All rights reserved.
//

import Model

public typealias NetworkingHandler = (Result<Data, NetworkingError>) -> Void

public protocol NetworkingProtocol {
    func run(_ request: URLRequest, completion handler: @escaping NetworkingHandler)
}
