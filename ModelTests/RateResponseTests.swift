//
//  RateResponseTests.swift
//  ModelTests
//
//  Created by Alexandre Camilleri on 19/02/2019.
//  Copyright © 2019 Alexandre Camilleri. All rights reserved.
//

import XCTest
@testable import Model

class RateResponseTests: XCTestCase {
    
    func testInitializeRateResponse() {
        let decoder = JSONDecoder()
        do {
            let rateResponse = try decoder.decode(RateResponse.self,
                                                  from: Fixture.pairRateResponseDataJson)
            
            XCTAssertEqual(rateResponse.httpStatusCode, .ok)
            
            guard let eurgbpRate = rateResponse.rates["EURGBP"] else {
                XCTFail("can't retrieve EURGBP rate")
                return
            }
            XCTAssertEqual(eurgbpRate.value, 0.891724)
            XCTAssertEqual(eurgbpRate.updated, Date(timeIntervalSinceReferenceDate: 1532429549281))
            
            guard let usdjpyRate = rateResponse.rates["USDJPY"] else {
                XCTFail("can't retrieve USDJPY rate")
                return
            }
            XCTAssertEqual(usdjpyRate.value, 111.1307)
            XCTAssertEqual(usdjpyRate.updated, Date(timeIntervalSinceReferenceDate: 1532429549281))
        } catch {
            XCTFail("error while decoding data: \(error)")
        }
    }
    
    func testMissingParameter() {
        let decoder = JSONDecoder()
        do {
            let rateResponse = try decoder.decode(RateResponse.self,
                                                  from: Fixture.argumentMissingResponseDataJson)
            
            XCTAssertEqual(rateResponse.httpStatusCode, .missingRequestParameter)
            XCTAssertEqual(rateResponse.message, "'pairs' parameter is required")
        } catch {
            XCTFail("error while decoding data: \(error)")
        }
    }
    
    func testUnrecognizedPair() {
        let decoder = JSONDecoder()
        do {
            let rateResponse = try decoder.decode(RateResponse.self,
                                                  from: Fixture.unrecognizedPairResponseDataJson)
            
            XCTAssertEqual(rateResponse.httpStatusCode, .unrecognizedPair)
            XCTAssertEqual(rateResponse.message, "The currency pair 'USDABC' was not recognised or supported")
        } catch {
            XCTFail("error while decoding data: \(error)")
        }
    }
}
