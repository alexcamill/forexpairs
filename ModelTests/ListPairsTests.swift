//
//  ListPairsTests.swift
//  ModelTests
//
//  Created by Alexandre Camilleri on 19/02/2019.
//  Copyright © 2019 Alexandre Camilleri. All rights reserved.
//

import XCTest
@testable import Model

class ListPairsTests: XCTestCase {
    
    func testInitializeCurrencyPairResponse() {
        let decoder = JSONDecoder()
        do {
            let currencyPairsListResponse = try decoder.decode(CurrencyPairsListResponse.self,
                                                               from: Fixture.currencyPairsListResponseDataJson)
            let pairs = currencyPairsListResponse.currencyPairs
            
            XCTAssertEqual(currencyPairsListResponse.httpStatusCode, .missingRequestParameter)
            
            pairs.forEach { pair in XCTAssertTrue(Fixture.supportedPairs.contains(pair), "Pair not found \(pair)") }
        } catch {
            XCTFail("error while decoding data: \(error)")
        }
    }
}
