//
//  Fixtures.swift
//  ModelTests
//
//  Created by Alexandre Camilleri on 19/02/2019.
//  Copyright © 2019 Alexandre Camilleri. All rights reserved.
//

import Foundation

struct Fixture {
    
    /// MARK: - Pairs listing Response
    
//    {
//    "supportedPairs":[
//    "EURUSD",
//    "EURGBP",
//    "GBPUSD",
//    "USDJPY",
//    "AUDUSD",
//    "USDCHF",
//    "NZDUSD",
//    "USDCAD",
//    "USDZAR"
//    ],
//    "message":"'pairs' parameter is required",
//    "code":1001
//    }
    static let currencyPairsListResponseDataJson = Data(base64Encoded: """
eyJzdXBwb3J0ZWRQYWlycyI6WyJFVVJVU0QiLCJFVVJHQlAiL\
CJHQlBVU0QiLCJVU0RKUFkiLCJBVURVU0QiLCJVU0RDSEYiLC\
JOWkRVU0QiLCJVU0RDQUQiLCJVU0RaQVIiXSwibWVzc2FnZSI\
6IidwYWlycycgcGFyYW1ldGVyIGlzIHJlcXVpcmVkIiwiY29k\
ZSI6MTAwMX0=
""")!
    
    static let supportedPairs = [
        "EURUSD",
        "EURGBP",
        "GBPUSD",
        "USDJPY",
        "AUDUSD",
        "USDCHF",
        "NZDUSD",
        "USDCAD",
        "USDZAR"
    ]
    
    /// MARK: - Rate Responses
    
//    {
//        "rates":{
//            "EURGBP":{
//                "rate":0.891724,
//                "timestamp":1532429549281
//            },
//            "USDJPY":{
//                "rate":111.1307,
//                "timestamp":1532429549281
//            }
//        },
//        "code":200
//    }
    static let pairRateResponseDataJson = Data(base64Encoded:"""
eyJyYXRlcyI6eyJFVVJHQlAiOnsicmF0ZSI6MC44OTE3MjQsI\
nRpbWVzdGFtcCI6MTUzMjQyOTU0OTI4MX0sIlVTREpQWSI6ey\
JyYXRlIjoxMTEuMTMwNywidGltZXN0YW1wIjoxNTMyNDI5NTQ\
5MjgxfX0sImNvZGUiOjIwMH0=
""")!
    
//    {
//        "message":"'pairs' parameter is required",
//        "code":1001
//    }
    static let argumentMissingResponseDataJson = Data(base64Encoded: """
eyJtZXNzYWdlIjoiJ3BhaXJzJyBwYXJhbWV0ZXIgaXMgcmVxd\
WlyZWQiLCJjb2RlIjoxMDAxfQ==
""")!
    
//    {
//        "message":"The currency pair 'USDABC' was not recognised or supported",
//        "code":1002
//    }
    static let unrecognizedPairResponseDataJson = Data(base64Encoded: """
eyJtZXNzYWdlIjoiVGhlIGN1cnJlbmN5IHBhaXIgJ1VTREFCQ\
ycgd2FzIG5vdCByZWNvZ25pc2VkIG9yIHN1cHBvcnRlZCIsIm\
NvZGUiOjEwMDJ9
""")!

}


