//
//  NetworkingTests.swift
//  NetworkingTests
//
//  Created by Alexandre Camilleri on 19/02/2019.
//  Copyright © 2019 Alexandre Camilleri. All rights reserved.
//

import XCTest
@testable import Networking

class NetworkingTests: XCTestCase {
    
    let freeForexApiUrlRequest = URLRequest(url: URL(string: "https://www.freeforexapi.com/api/live")!)
    
    let freeForexApiEURUSDPairUrlRequest = URLRequest(url: URL(string: "https://www.freeforexapi.com/api/live?pairs=EURUSD")!)

    func testGet() {
        let networking = Networking()
        var data: Data?
        let expectation = self.expectation(description: "result data")
        
        networking.run(freeForexApiUrlRequest) { result in
            switch result {
            case .success(let requestData):
                print(requestData.base64EncodedString())
                data = requestData
                expectation.fulfill()
            case .failure(let error):
                XCTFail("no data retrieved: \(error)")
            }
        }
        waitForExpectations(timeout: 2, handler: nil)
        XCTAssertNotNil(data)
    }
    
    func testGetWithParameter() {
        let networking = Networking()
        var data: Data?
        let expectation = self.expectation(description: "result data")
        
        networking.run(freeForexApiEURUSDPairUrlRequest) { result in
            switch result {
            case .success(let requestData):
                print(requestData.base64EncodedString())
                data = requestData
                expectation.fulfill()
            case .failure(let error):
                XCTFail("no data retrieved: \(error)")
            }
        }
        waitForExpectations(timeout: 2, handler: nil)
        XCTAssertNotNil(data)
    }
}
