//
//  Rate.swift
//  Model
//
//  Created by Alexandre Camilleri on 19/02/2019.
//  Copyright © 2019 Alexandre Camilleri. All rights reserved.
//

public struct Rate {
    /// Trading value a currency Pair
    public let value: Double
    /// Update date
    public let updated: Date
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        value = try container.decode(Double.self, forKey: CodingKeys.value)
        updated = try container.decode(Date.self, forKey: CodingKeys.updated)
    }
}

extension Rate: Decodable {
    enum CodingKeys: String, CodingKey {
        case value = "rate"
        case updated = "timestamp"
    }
}
