//
//  RateResponse.swift
//  Model
//
//  Created by Alexandre Camilleri on 19/02/2019.
//  Copyright © 2019 Alexandre Camilleri. All rights reserved.
//

public typealias CurrencyPairsRate = [CurrencyPairName: Rate]

// Rate response received from `freeforexapi.com`
public struct RateResponse {
    public var httpStatusCode: HttpStatusCode
    public var message: String?
    public var rates: CurrencyPairsRate
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        httpStatusCode = try container.decode(HttpStatusCode.self, forKey: CodingKeys.httpStatusCode)
        message = try container.decodeIfPresent(String.self, forKey: CodingKeys.message)
        rates = try container.decodeIfPresent([CurrencyPairName: Rate].self, forKey: CodingKeys.rates) ?? [:]
    }
}

extension RateResponse: Decodable {
    enum CodingKeys: String, CodingKey {
        case httpStatusCode = "code"
        case message
        case rates
    }
}

