//
//  CurrencyPairsList.swift
//  Model
//
//  Created by Alexandre Camilleri on 19/02/2019.
//  Copyright © 2019 Alexandre Camilleri. All rights reserved.
//

public typealias CurrencyPairName = String

// Rate response received from `freeforexapi.com`
public struct CurrencyPairsListResponse {
    public var httpStatusCode: HttpStatusCode
    public var message: String?
    public var currencyPairs: [CurrencyPairName]
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        httpStatusCode = try container.decode(HttpStatusCode.self,  forKey: CodingKeys.httpStatusCode)
        message = try container.decodeIfPresent(String.self, forKey: CodingKeys.message)
        currencyPairs = try container.decodeIfPresent([CurrencyPairName].self, forKey: CodingKeys.currencyPairs) ?? []
    }
}

extension CurrencyPairsListResponse: Decodable {
    enum CodingKeys: String, CodingKey {
        case httpStatusCode = "code"
        case message
        case currencyPairs = "supportedPairs"
    }
}

