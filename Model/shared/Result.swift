//
//  Result.swift
//  Model
//
//  Created by Alexandre Camilleri on 19/02/2019.
//  Copyright © 2019 Alexandre Camilleri. All rights reserved.
//

#warning("remove when updating to Swift 5")
public enum Result<Value, Error: Swift.Error> {
    case success(Value)
    case failure(Error)
}
